#include <cstdio>

int cost(int distance) {
  return distance * distance;
}

int main() {
  const int N = 30;
  int d[N][N], f[N][N];

  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      scanf("%d", &d[i][j]);

  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      if (d[i][j] < 0) {
        fprintf(stderr, "error i=%d j=%d d[i][j]=%d\n", i, j, d[i][j]);
        return 1;
      }

  for (int i = 0; i < N; i++)
    if (d[i][i] != 0) {
      fprintf(stderr, "error i=%d d[i][i]=%d\n", i, d[i][i]);
      return 2;
    }

  for (int i = 0; i < N; i++)
    for (int j = 0; j < N; j++)
      f[i][j] = cost(d[i][j]);

  int c = 0;
  for (int i = 0; i < N; i++)
    for (int j = 0; j < i; j++)
      for (int k = 0; k < N; k++)
        if (f[i][j] > f[i][k] + f[k][j]) {
          printf("i=%d\tj=%d\tf=%d\td=%d\n"
                 "i=%d\tk=%d\tf=%d\td=%d\n"
                 "k=%d\tj=%d\tf=%d\td=%d\n\n",
                 i, j, f[i][j], d[i][j],
                 i, k, f[i][k], d[i][k],
                 k, j, f[k][j], d[k][j]);
          c++;
        }
  printf("c=%d\n", c);

  return 0;
}
