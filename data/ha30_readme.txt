http://people.sc.fsu.edu/~jburkardt/datasets/cities/cities.html

HA30 describes the airline distances, in hundreds of miles, between 30 international cities. For this problem, the spherical geometry is significantly different from the planar approximation.

ha30_main.txt, is the main file.
ha30_code.txt, a code for each city.
ha30_dist.txt, the city-to-city distance matrix.
ha30_xyz.txt, XYZ coordinates (in hundreds of miles) that can be assigned to the cities, assuming the earth has a radius of 3,959 miles, so that the great circle distances are approximately those given in the distance table, estimated by the MATLAB program "distance_to_position_sphere".
ha30_name.txt, the name of each city.
