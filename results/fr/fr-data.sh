#!/bin/bash
ts='2012-11-14-01-43-30'
echo "runs 3"
for n in `seq 10 5 40`; do
  # # of agents
  echo "n $n"
  # CPU time
  echo -n "cpu "
  grep -h -o -P '(?<=client_monitor: )\d+(?= ms)' fr.$ts.n$n.run?.log
  # Peak memory
  echo -n "mem "
  grep -h -o -P '\d+(?= bytes \(peak\))' fr.$ts.n$n.run?.log
  # Max # of worlds
  echo -n "worlds "
  grep -h -o -P '(?<=Limit=1000000, Max=)\d+' fr.$ts.n$n.run?.log
  for r in `seq 1 3`; do
    # Start time
    echo -n "start "
    grep -o -P '(?<=Start_Time=)\d+' fr.$ts.n$n.run$r.log
    echo -n "count "
    grep 'arrived at' fr.$ts.n$n.run$r.log | wc -l
    grep 'arrived at' fr.$ts.n$n.run$r.log | while read line; do
      echo $line | grep -o -P '(?<=Traveler )\d+'
      echo $line | grep -o -P '(?<=at )\d+'
    done
  done
done
