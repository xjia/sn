#include <cstdio>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, char **argv) {
  if (argc > 1) {
    freopen(argv[1], "r", stdin);
  }

  string xxx;
  cout << "n\tcpuA\tcpuMa\tcpuMi\twrdA\twrdMa\twrdMi\tmemA\tmemMa\tmemMi" << endl;

  vector<int> savings(11, 0);
  
  int runs; cin >> xxx >> runs;

  for (int n; cin >> xxx >> n;) {
    cin >> xxx;
    double avg_cpu = 0, max_cpu = 0, min_cpu = 1e9;
    for (int run = 1; run <= runs; run++) {
      double cpu; cin >> cpu; cpu /= 1000;
      avg_cpu += cpu;
      max_cpu = max(max_cpu, cpu);
      min_cpu = min(min_cpu, cpu);
    }
    avg_cpu /= runs;

    cin >> xxx;
    double avg_mem = 0, max_mem = 0, min_mem = 1e9;
    for (int run = 1; run <= runs; run++) {
      double mem; cin >> mem; mem /= 1024 * 1024;
      avg_mem += mem;
      max_mem = max(max_mem, mem);
      min_mem = min(min_mem, mem);
    }
    avg_mem /= runs;

    cin >> xxx;
    double avg_wrd = 0, max_wrd = 0, min_wrd = 1e9;
    for (int run = 1; run <= runs; run++) {
      double wrd; cin >> wrd;
      avg_wrd += wrd;
      max_wrd = max(max_wrd, wrd);
      min_wrd = min(min_wrd, wrd);
    }
    avg_wrd /= runs;

    for (int run = 1; run <= runs; run++) {
      double start_time; cin >> xxx >> start_time;
      int line_count; cin >> xxx >> line_count;

      double exit_time[1000];
      for (int agent_i = 1; agent_i <= n; agent_i++) {
        exit_time[agent_i] = 0;
      }
      double last_exit_time = 1e-6;

      for (int k = 1; k <= line_count; k++) {
        int agent_i; double down_time;
        cin >> agent_i >> down_time;
        exit_time[agent_i] = max(exit_time[agent_i], down_time - start_time);
        last_exit_time = max(last_exit_time, exit_time[agent_i]);
      }
      for (int agent_i = 1; agent_i <= n; agent_i++) {
        double time_saving = (last_exit_time - exit_time[agent_i]) / last_exit_time;
        int slot = time_saving * 10 + 0.5;
        savings[slot]++;
      }
    }

    cout << n << "\t"
         << avg_cpu << "\t" << max_cpu << "\t" << min_cpu << "\t"
         << avg_wrd << "\t" << max_wrd << "\t" << min_wrd << "\t"
         << avg_mem << "\t" << max_mem << "\t" << min_mem << endl;
  }

  for (int slot = 0; slot <= 10; slot++) {
    cout << (slot / 10.0) << "\t" << savings[slot] << endl;
  }

  return 0;
}
