#include <cstdio>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main(int argc, char **argv) {
  if (argc > 1) {
    freopen(argv[1], "r", stdin);
  }

  string xxx;

  int runs; cin >> xxx >> runs;

  for (int n; cin >> xxx >> n;) {
    vector<double> cpus, mems, wrds;

    cin >> xxx;
    for (int run = 1; run <= runs; run++) {
      double cpu; cin >> cpu; cpu /= 1000;
      cpus.push_back(cpu);
    }

    cin >> xxx;
    for (int run = 1; run <= runs; run++) {
      double mem; cin >> mem; mem /= 1024 * 1024;
      mems.push_back(mem);
    }

    cin >> xxx;
    for (int run = 1; run <= runs; run++) {
      double wrd; cin >> wrd;
      wrds.push_back(wrd);
    }

    for (int run = 1; run <= runs; run++) {
      const int i = run - 1;
      cout << n << "\t" << cpus[i] << "\t" << mems[i] << "\t" << wrds[i] << endl;
    }

    for (int run = 1; run <= runs; run++) {
      double start_time; cin >> xxx >> start_time;
      int line_count; cin >> xxx >> line_count;

      for (int k = 1; k <= line_count; k++) {
        int agent_i; double down_time;
        cin >> agent_i >> down_time;
      }
    }
  }

  return 0;
}
