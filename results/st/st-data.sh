#!/bin/bash
ts='2012-11-14-15-12-30'
runs=10
echo "runs $runs"
for n in `seq 3 3 15`; do
  # # of agents
  echo "n $n"
  # CPU time
  echo -n "cpu "
  grep -h -o -P '(?<=client_monitor: )\d+(?= ms)' st.$ts.n$n.run*.log
  # Peak memory
  echo -n "mem "
  grep -h -o -P '\d+(?= bytes \(peak\))' st.$ts.n$n.run*.log
  # Max # of worlds
  echo -n "worlds "
  grep -h -o -P '(?<=Limit=1000000, Max=)\d+' st.$ts.n$n.run*.log
  for r in `seq 1 $runs`; do
    # Start time
    echo -n "start "
    grep -o -P '(?<=Start_Time=)\d+' st.$ts.n$n.run$r.log
    echo -n "count "
    grep 'Down_Time' st.$ts.n$n.run$r.log | wc -l
    grep 'Down_Time' st.$ts.n$n.run$r.log | while read line; do
      echo $line | grep -o -P '(?<=Trader )\d+'
      echo $line | grep -o -P '(?<=Down_Time=)\d+'
    done
  done
done
