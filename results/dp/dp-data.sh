#!/bin/bash
ts='2012-11-14-10-39-50'
runs=10
echo "runs $runs"
for n in `seq 10 10 100`; do
  # # of agents
  echo "n $n"
  # CPU time
  echo -n "cpu "
  grep -h -o -P '(?<=client_monitor: )\d+(?= ms)' dp.$ts.n$n.run*.log
  # Peak memory
  echo -n "mem "
  grep -h -o -P '\d+(?= bytes \(peak\))' dp.$ts.n$n.run*.log
  # Max # of worlds
  echo -n "worlds "
  grep -h -o -P '(?<=Limit=1000000, Max=)\d+' dp.$ts.n$n.run*.log
  for r in `seq 1 $runs`; do
    # Start time
    echo -n "start "
    grep -o -P '(?<=Start_Time=)\d+' dp.$ts.n$n.run$r.log
    echo -n "count "
    grep 'Down_Time' dp.$ts.n$n.run$r.log | wc -l
    grep 'Down_Time' dp.$ts.n$n.run$r.log | while read line; do
      echo $line | grep -o -P '(?<=DP )\d+'
      echo $line | grep -o -P '(?<=Down_Time=)\d+'
    done
  done
done
