PROC_LIMIT = 100000000
NODE_NAME = mynode
EVAL = init:stop().
S = 4:4

# Usage example: 
# 	make NODE_NAME=compg0 EVAL='test:bj(1).'

compile:
	rebar compile

run: compile
	erl -sname $(NODE_NAME) -pa ebin +P $(PROC_LIMIT)

test: compile
	time -p erl -sname $(NODE_NAME) -pa ebin +P $(PROC_LIMIT) -eval "$(EVAL)" -noinput +S $(S)

bj:
	make test EVAL="test:bj($(N))."

dp:
	make test EVAL="test:dp($(N))."

fr:
	make test EVAL="test:fr($(N))."

st:
	make test EVAL="test:st($(N))."

all_dp:
	./scripts/dp.sh

all_fr:
	./scripts/fr.sh

all_st:
	./scripts/st.sh

sim_dp:
	make test EVAL="sim_test:dp($(N),$(SIM))."

sim_fr:
	make test EVAL="sim_test:fr($(N),$(SIM))."

sim_st:
	make test EVAL="sim_test:st($(N),$(SIM))."

all_sim_dp:
	./scripts/sim_dp.sh

all_sim_fr:
	./scripts/sim_fr.sh

all_sim_st:
	./scripts/sim_st.sh

clean:
	rebar clean
