import Schedules

data Program a = In a (a -> Program a)
               | Rd a (a -> Program a)
               | Out a (Program a)
               | Choice (Program a) (Program a)
               | Commit (Program a)
               | EmptyProgram

instance Schedulable (Program a) where
  isFinal EmptyProgram = True
  isFinal _            = False

type TupleSpace a = [a]

newTupleSpace :: TupleSpace a
newTupleSpace = []

data SimResult = Terminated | Stuck deriving (Show)

--------------------------------------------------------------------------------

data DpTuple = Ready | Fork Int deriving (Eq, Show)

dpInit :: Int -> Program DpTuple
dpInit n = mkInit 0 n
           where mkInit i n | i == n    = Out Ready EmptyProgram
                            | otherwise = Out (Fork i) (mkInit (i + 1) n)

dp :: Int -> Int -> Program DpTuple
dp i n = let l = i - 1
             r = rem i n
         in Rd Ready (\_ -> Choice (grab l r) (grab r l))
         where grab x y = In (Fork x) (\_ ->
                          In (Fork y) (\_ ->
                          Out (Fork x) (
                          Out (Fork y) (
                          Commit EmptyProgram))))

testDp :: Int -> [Program DpTuple]
testDp n = (dpInit n):[ dp i n | i <- [1..n] ]

--------------------------------------------------------------------------------

cc :: (Eq a) => (Scheduler s) => [Program a] -> s -> (SimResult, [TupleSpace a])
cc programs scheduler = run s 0 ts [ts]
  where
    s = addAll programs scheduler
    ts = newTupleSpace

    run :: (Eq a) => (Scheduler s) => s -> Int -> TupleSpace a -> [TupleSpace a] -> (SimResult, [TupleSpace a])
    run s 0 ts tss | empty s = (Stuck, tss)
    run s step ts tss = case next s of
      (Nothing, _)                       -> (Terminated, tss)
      (Just (Schedule EmptyProgram), s') -> run s' (step + 1) ts tss
      (Just (Schedule p), s')
        | executable p ts -> run (add p' s) (step + 1) ts' (tss ++ [ts'])
        | otherwise       -> run (add p  s)  step      ts   tss
        where (p', ts') = execute p ts

    executable :: (Eq a) => Program a -> TupleSpace a -> Bool
    executable (In t _) ts = matches t ts
    executable (Rd t _) ts = matches t ts
    executable _        ts = True

    execute :: (Eq a) => Program a -> TupleSpace a -> (Program a, TupleSpace a)
    execute (In t next) ts = (p', ts')
      where
        t'  = match t ts
        ts' = remove t' ts
        p'  = next t'

    execute (Rd t next) ts = (p', ts)
      where p' = next (match t ts)

    execute (Out t p) ts = (p, t:ts)

    execute (Choice p p') ts = (p, ts) -- it's determinisc at present

    execute (Commit p) ts = (p, ts)    -- don't have choice now, ignore

    matches :: (Eq a) => a -> TupleSpace a -> Bool
    matches _ []                  = False
    matches t (t':ts) | t == t'   = True
                      | otherwise = matches t ts

    match :: (Eq a) => a -> TupleSpace a -> a
    match t (t':ts) | t == t'   = t'
                    | otherwise = match t ts

    remove :: (Eq a) => a -> TupleSpace a -> TupleSpace a
    remove t (t':ts) | t == t'   = ts
                     | otherwise = t':(remove t ts)

--------------------------------------------------------------------------------

data Tree a = Leaf a
            | Branch (Tree a) (Tree a)

data World a = World [Program a] (TupleSpace a) -- no exit snapshots at present

--sn :: (Eq a) => [Program a] -> (SimResult, Tree (World a))
--sn programs = run (World programs newTupleSpace)

--------------------------------------------------------------------------------

main :: IO ()
main = putStrLn $ show $ cc (testDp 3) newRoundRobin
