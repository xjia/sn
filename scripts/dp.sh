#!/bin/bash
#
ts=`date +%Y-%m-%d-%H-%M-%S`
echo $ts
#
for i in `seq 10 10 100`; do
  echo $i
  for j in `seq 1 10`; do
    sleep 5
    echo $j
    make dp N=$i 1>dp.$ts.n$i.run$j.log 2>&1
  done
done
