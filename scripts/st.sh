#!/bin/bash
#
ts=`date +%Y-%m-%d-%H-%M-%S`
echo $ts
#
for i in `seq 5 5 50`; do
  echo $i
  for j in `seq 1 3`; do
    sleep 5
    echo $j
    make st N=$i 1>st.$ts.n$i.run$j.log 2>&1
  done
done
