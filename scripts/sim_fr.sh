#!/bin/bash
ts=`date +%Y-%m-%d-%H-%M-%S`
echo $ts
for i in `seq 1 10`; do
  echo "N=$i"
  for j in `seq 1 10`; do
    echo $j
    make sim_fr N=$i SIM=sim_cc 1>sim_cc.fr.$ts.n$i.run$j.log 2>&1
    make sim_fr N=$i SIM=sim_gcc 1>sim_gcc.fr.$ts.n$i.run$j.log 2>&1
  done
done
