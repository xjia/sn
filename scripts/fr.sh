#!/bin/bash
#
ts=`date +%Y-%m-%d-%H-%M-%S`
echo $ts
#
for i in `seq 10 5 50`; do
  echo $i
  for j in `seq 1 1 3`; do
    sleep 5
    echo $j
    make fr N=$i 1>fr.$ts.n$i.run$j.log 2>&1
  done
done
