-module(tbuy).
-export([start/1]).
-import(sn, [speculate/2, cm/1, in/2, out/2]).
-include("sn.hrl").

start(State={I, {X, Y, Cash, [Y,Z|_]}}) ->
  io:format("Traveler ~p: ~p~n", [I, State]),
  timer:sleep(random:uniform(100)),
  speculate(
    fun(_) ->
      in({ticket, X, Y, ?LessEqual(Cash)}, fun(_, _) ->
        cm(fun(_) ->
          {Down_Time, _} = statistics(runtime),
          io:format("Traveler ~p: arrived at ~p by XY~n", [I, Down_Time]),
          out({arrived, I}, fun(_) -> ok end)
        end)
      end)
    end,
    fun(_) ->
      XCash = Cash / 0.9,
      in({ticket, X, Z, ?LessEqual(XCash)}, fun({_, _, _, Cost}, _) ->
        YCash = (Cash - Cost) / 0.9,
        in({ticket, Z, Y, ?LessEqual(YCash)}, fun(_, _) ->
          cm(fun(_) ->
            {Down_Time, _} = statistics(runtime),
            io:format("Traveler ~p: arrived at ~p by XZY~n", [I, Down_Time]),
            out({arrived, I}, fun(_) -> ok end)
          end)
        end)
      end)
    end).
