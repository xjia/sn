-module(sn_app).
-export([start/0, loop/0]).
-export([start/1, start/2, requires/1]).

start() ->
  case whereis(?MODULE) of
    undefined ->
      Pid = spawn(?MODULE, loop, []),
      true = register(?MODULE, Pid),
      io:format("~p registered~n", [?MODULE]),
      {started, Pid}
  ; Pid when is_pid(Pid) ->
      {running, Pid}
  end.

loop() ->
  receive
    {start, Module, Pid} when is_pid(Pid) ->
      Pid ! {ok, Module:start()}
  ; {start, Module, Param, Pid} when is_pid(Pid) ->
      Pid ! {ok, Module:start(Param)}
  end,
  timer:sleep(100),
  loop().

%%%

start(Module) ->
  sn_app:start(),
  whereis(?MODULE) ! {start, Module, self()},
  receive
    {ok, Result} -> Result
  end.

start(Module, Param) ->
  sn_app:start(),
  whereis(?MODULE) ! {start, Module, Param, self()},
  receive
    {ok, Result} -> Result
  end.

requires(Name) when is_atom(Name) ->
  case whereis(Name) of
    undefined ->
      timer:sleep(100),
      io:format("try again for ~p~n", [Name]),
      requires(Name)
  ; Pid when is_pid(Pid) ->
      ok
  end.
