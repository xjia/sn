-module(tbuy_finalizer).
-export([start/1, loop/1]).
-include("sn.hrl").

start(N) -> spawn(?MODULE, loop, [N]).

loop(0) -> io:format("sleep before finalize~n"),
           timer:sleep(5000),
           io:format("finalize!~n"),
           whereis(client_monitor) ! finalize;

loop(N) -> sn:in({arrived, ?Any}, fun(_, _) ->
             loop(N-1)
           end).
