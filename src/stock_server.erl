-module(stock_server).
-export([start/3]).
-import(sn, [in/2, out/2]).
-include("sn.hrl").

-define(TICK, 150).

start(Stock, Start_Quantity, Prices=[P|_]) ->
  spawn(fun() ->
    spawn_link(fun() -> ticker(Stock) end),
    out({price, Stock, P}, fun(_) ->
      serve(Stock, Start_Quantity, Prices, 0, [], 1)
    end)
  end).

serve(X, Q, [], K, Passed, Day) ->
  io:format("Stock ~p price reverses @ ~p, day ~p~n", [X, Q, Day]),
  serve(X, Q, Passed, K, [], Day);

serve(X, Q, [P|Ps=[NewPrc|_]], 1, Passed, Day) ->
  in({price, X, ?Any}, fun(_, _) ->
    out({price, X, NewPrc}, fun(_) ->
      serve(X, Q, Ps, 0, [P|Passed], Day+1)
    end)
  end);

serve(X, Q, Ps=[P|_], K, Passed, Day) ->
  in({?Any, X, ?Any, ?Any}, fun
    ({tick, _, _, _}, _) ->
      out({ack, tick}, fun(_) ->
        serve(X, Q, Ps, K+1, Passed, Day)
      end)
  ; ({buy, _, Q1, Me}, _) ->
      Q2 = min(Q1, Q),
      out({ack, Me, P, Q2}, fun(_) ->
        serve(X, Q-Q2, Ps, K+1, Passed, Day)
      end)
  ; ({sell, _, Q2, Me}, _) ->
      out({ack, Me, P}, fun(_) ->
        serve(X, Q+Q2, Ps, K, Passed, Day)
      end)
  end).

ticker(X) ->
  T = ?TICK + random:uniform(10) * 10,
  receive
    after T ->
      out({tick, X, X, X}, fun(_) ->
        in({ack, tick}, fun(_, _) -> ticker(X) end)
      end)
  end.
