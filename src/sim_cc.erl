-module(sim_cc).  % simulation of committed choice
-export([start/0, loop/1, schedule/0]).
-export([start/2, choice/2, in/2, rd/2, out/2, cm/1]).

start() ->
  Pid = spawn(?MODULE, loop, init()),
  register(?MODULE, Pid),
  Pid.

init() -> [_Processes = []].

loop(Ps) ->
  receive
    {start, Module, Param} ->
      {P,_} = spawn_monitor(Module, start, [Param]),
      loop(lists:append(Ps,[P]))
  ; schedule ->
      {A1,A2,A3} = now(),
      random:seed(A1, A2, A3),
      loop(Ps, [], [], 0)
  end.

schedule() -> ?MODULE ! schedule.

loop([], Ts, [], _) ->
  io:format("finalize: Ts=~p~n", [Ts]),
  init:stop();

loop([], Ts, _, 0) ->
  io:format("stuck: Ts=~p~n", [Ts]),
  init:stop();

loop([], Ts, Px, _) ->
  loop(Px, Ts, [], 0);

loop([P|Ps], Ts, Px, Progress) ->
  receive
    {start, Module, Param} ->
      {S,_} = spawn_monitor(Module, start, [Param]),
      loop(lists:append([P|Ps],[S]), Ts, Px, Progress + 1)
  ; {'DOWN', _Ref, process, Pid, _Info} ->
      loop(lists:delete(Pid,[P|Ps]), Ts, lists:delete(Pid,Px), Progress + 1)
  after 10 ->
    P ! next_step,
    receive
      choice ->
        % io:format("choice~n"),
        P ! random:uniform(2),
        loop(Ps, Ts, lists:append(Px,[P]), Progress + 1)
    ; {in, Template} ->
        % io:format("{in, ~p}~n", [Template]),
        case util:match_tuple(Template, Ts) of
          {ok, Tuple} ->
            P ! {ok, Tuple},
            loop(Ps, lists:delete(Tuple,Ts), lists:append(Px,[P]), Progress + 1)
        ; nothing ->
            P ! {error, wait},
            loop(Ps, Ts, lists:append(Px,[P]), Progress)
        end
    ; {rd, Template} ->
        % io:format("{rd, ~p}~n", [Template]),
        case util:match_tuple(Template, Ts) of
          {ok, Tuple} ->
            P ! {ok, Tuple},
            loop(Ps, Ts, lists:append(Px,[P]), Progress + 1)
        ; nothing ->
            P ! {error, wait},
            loop(Ps, Ts, lists:append(Px,[P]), Progress)
        end
    ; {out, Tuple} ->
        % io:format("{out, ~p}~n", [Tuple]),
        loop(Ps, lists:append(Ts,[Tuple]), lists:append(Px,[P]), Progress + 1)
    end
  end.

%%%

start(Module, Param) -> ?MODULE ! {start, Module, Param}.

choice(L, R) ->
  receive
    next_step ->
      ?MODULE ! choice,
      receive
        1 -> L()
      ; 2 -> R()
      end
  end.

in(Template, Next) ->
  receive
    next_step ->
      ?MODULE ! {in, Template},
      receive
        {ok, Tuple}   -> Next(Tuple)
      ; {error, wait} -> in(Template, Next)
      end
  end.

rd(Template, Next) ->
  receive
    next_step ->
      ?MODULE ! {rd, Template},
      receive
        {ok, Tuple}   -> Next(Tuple)
      ; {error, wait} -> rd(Template, Next)
      end
  end.

out(Tuple, Next) ->
  receive
    next_step ->
      ?MODULE ! {out, Tuple},
      Next()
  end.

cm(Next) -> Next().
