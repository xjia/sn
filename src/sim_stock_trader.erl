-module(sim_stock_trader).
-export([start/1]).
-include("sn.hrl").

-define(Start, 5000.0).
-define(Goal,  5500.0).

start({I, Left_Alpha, Right_Alpha, Stocks, Sim}) ->
  Sim:choice(fun() -> trade(I, ?Start, Left_Alpha,  Stocks, 1, Sim) end,
             fun() -> trade(I, ?Start, Right_Alpha, Stocks, 1, Sim) end).

trade(I, Cash, Alpha, [X|Xs], Round, Sim) ->
  Sim:rd({price, X, ?Any}, fun({_, _, P1}) ->
    Q1 = util:floor(Cash / P1),
    Sim:out({buy, X, Q1, I}, fun() ->
      Sim:in({ack, I, ?Any, ?Any}, fun({_, _, P2, Q2}) ->
        C1 = Cash - P2 * Q2,
        Sim:rd({price, X, price_condition(Alpha, P2)}, fun(_) ->
          Sim:out({sell, X, Q2, I}, fun() ->
            Sim:in({ack, I, ?Any}, fun({_, _, P3}) ->
              C2 = C1 + P3 * Q2,
              case C2 < ?Goal andalso Round < 3 of
                true ->
                  trade(I, C2, Alpha, Xs++[X], Round+1, Sim)
              ; false ->
                  Sim:cm(fun() -> ok end)
              end
            end)
          end)
        end)
      end)
    end)
  end).

price_condition(Alpha, P2) ->
  fun(X) ->
    (X >= (1 + Alpha)*P2) orelse (X =< (1 - Alpha/2)*P2)
  end.
