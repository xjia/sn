-module(memory_monitor).
-export([start/0, loop/2]).

-define(INTERVAL, 100).

start() ->
  case whereis(?MODULE) of
    undefined ->
      Memory = erlang:memory(total),
      Pid = spawn(?MODULE, loop, [_Last = Memory, _Peak = Memory]),
      true = register(?MODULE, Pid),
      io:format("~p registered~n", [?MODULE]),
      {started, Pid}
  ; Pid when is_pid(Pid) ->
      {running, Pid}
  end.

loop(Last, Peak) when Last > Peak ->
  loop(Last, Last);
loop(Last, Peak) ->
  receive
    Pid when is_pid(Pid) ->
      Pid ! {memory, Last, Peak},
      loop(Last, Peak)
    after ?INTERVAL ->
      loop(erlang:memory(total), Peak)
  end.
