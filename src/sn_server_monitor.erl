-module(sn_server_monitor).
-export([start/1]).

start(Pid) when is_pid(Pid) ->
  spawn(fun() ->
    MonitorRef = monitor(process, Pid),
    receive
      {'DOWN', MonitorRef, process, Pid, Info} ->
        io:format("!!!!!!!!!!!!!!!!!!!!~nsn_server_monitor: Info=~p~n", [Info]),
        init:stop()
    end
  end).
