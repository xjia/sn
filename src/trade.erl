-module(trade).
-export([buy/2, sell/2]).
-import(sn, [in/2, rd/2, out/2]).
-include("sn.hrl").

sell(T, Next) ->
  Id = util:generate_unique_md5(),
  out({sell, T, Id}, fun(_) ->
    in({buy, T, Id}, fun(_, _) ->
      out({ack, T, Id}, fun(_) ->
        Next()
      end)
    end)
  end).

buy(T, Next) ->
  in({sell, T, ?Any}, fun({sell, _, Id}, _) ->
    out({buy, T, Id}, fun(_) ->
      rd({ack, T, Id}, fun(_, _) ->
        Next()
      end)
    end)
  end).