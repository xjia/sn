-module(stock_trader).
-export([start/1]).
-import(sn, [speculate/2, rd/2, out/2, in/2, cm/1]).
-include("sn.hrl").

-define(Start, 5000.0).
-define(Goal,  5500.0).

start({I, Left_Alpha, Right_Alpha, Stocks}) ->
  timer:sleep(random:uniform(150)),
  speculate(fun(_) -> trade(I, ?Start, Left_Alpha,  Stocks, 1) end,
            fun(_) -> trade(I, ?Start, Right_Alpha, Stocks, 1) end).

trade(I, Cash, Alpha, [X|Xs], Round) ->
  io:format("Trader ~p: Cash=~p X=~p Round=~p~n", [I, Cash, X, Round]),
  rd({price, X, ?Any}, fun({_, _, P1}, _) ->
    Q1 = util:floor(Cash / P1),
    io:format("Trader ~p: buy ~p ~p~n", [I, Q1, X]),
    out({buy, X, Q1, I}, fun(_) ->
      in({ack, I, ?Any, ?Any}, fun({_, _, P2, Q2}, _) ->
        C1 = Cash - P2 * Q2,
        io:format("Trader ~p: bought ~p ~p @ ~p~n", [I, Q2, X, P2]),
        rd({price, X, price_condition(Alpha, P2)}, fun(_, _) ->
          out({sell, X, Q2, I}, fun(_) ->
            in({ack, I, ?Any}, fun({_, _, P3}, _) ->
              C2 = C1 + P3 * Q2,
              case C2 < ?Goal andalso Round < 3 of
                true ->
                  trade(I, C2, Alpha, lists:append(Xs,[X]), Round+1)
              ; false ->
                  cm(fun(_) ->
                    {Down_Time, _} = statistics(runtime),
                    io:format("Trader ~p: Cash=~p Down_Time=~p~n", [I, C2, Down_Time]),
                    out({cash, I, C2}, fun(_) -> ok end)
                  end)
              end
            end)
          end)
        end)
      end)
    end)
  end).

price_condition(Alpha, P2) ->
  fun(X) ->
    (X >= (1 + Alpha)*P2) orelse (X =< (1 - Alpha/2)*P2)
  end.
