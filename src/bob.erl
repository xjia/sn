-module(bob).
-export([start/0]).
-import(sn, [speculate/2, cm/1]).
-import(trade, [buy/2, sell/2]).

start() ->
  speculate(
    fun(_) ->
      buy(goodlens, fun() ->
        sell(avglens, fun() ->
          cm(fun(_) ->
            io:format("@@@ bob: goodlens => avglens~n~n")
          end)
        end)
      end)
    end,
    fun(_) ->
      buy(goodcam, fun() ->
        sell(avgcam, fun() ->
          cm(fun(_) ->
            io:format("*** bob: goodcam => avgcam~n~n")
          end)
        end)
      end)
    end).