-module(sn).
-export([start/1, start/2, speculate/2, cm/1, in/2, rd/2, out/2]).

-define(INFO(_Format, _Params), ok).
% -define(INFO(Format, Params), io:format(Format, Params)).

%%%%%%%%%%%%%%%%%
%% client-side %%
%%%%%%%%%%%%%%%%%

start(Module) ->
  client_monitor:start(fun() -> Module:start() end).

start(Module, Param) ->
  client_monitor:start(fun() -> Module:start(Param) end).

speculate(Left_Choice, Right_Choice) ->
  Color = get_my_color(),
  tell_world({split, Color},
    fun([L, R]) ->
      ?INFO("                                   # split ok in ~p => [~p, ~p]~n", [get_current_world(), L, R]),
      client_monitor:spawn(fun() ->
        set_my_color(Color),
        set_current_world(L),
        Left_Choice(L)
      end),
      set_current_world(R),
%%%
% If want to implement cm timeout or cm on error/exception,
% should wrap the choice functions here and above.
%%%
      Right_Choice(R)
    end,
    fun() ->
      speculate(Left_Choice, Right_Choice)
    end).

cm(Next) ->
  tell_world({commit, get_my_color()},
    fun(Parent) ->
      ?INFO("                                   # cm ok in ~p => ~p~n", [get_current_world(), Parent]),
      set_current_world(Parent),
      Next(Parent)
    end,
    fun() ->
      cm(Next)
    end).

in(Template, Next) ->
  tell_world({in, Template},
    fun(Tuple) ->
      Next(Tuple, get_current_world())
    end,
    fun() ->
      in(Template, Next)
    end).

rd(Template, Next) ->
  tell_world({rd, Template},
    fun(Tuple) ->
      Next(Tuple, get_current_world())
    end,
    fun() ->
      rd(Template, Next)
    end).

out(Tuple, Next) ->
  tell_world({out, Tuple},
    fun() ->
      Next(get_current_world())
    end,
    fun() ->
      out(Tuple, Next)
    end).

%%%%%%%%%%%%%%%%%%%%%%
%% helper functions %%
%%%%%%%%%%%%%%%%%%%%%%

tell_world(Msg, Ok_Fun, Retry_Fun) ->
  case remote:send(get_current_world(), Msg) of
    ok ->
      delay:clear(),
      Ok_Fun()
  ; {ok, Param} ->
      delay:clear(),
      Ok_Fun(Param)
  ; {retry, [L, R]} ->
      ?INFO("                                   # ~p retry in ~p => [~p, ~p]~n", [Msg, get_current_world(), L, R]),
      Delay = delay:execute(),
      Color = get_my_color(),
      client_monitor:spawn(fun() ->
        delay:set_delay(Delay),
        set_my_color(Color),
        set_current_world(L),
        Retry_Fun()
      end),
      set_current_world(R),
      Retry_Fun()
  ; {retry, Referee} ->
      ?INFO("                                   # ~p retry in ~p => ~p~n", [Msg, get_current_world(), Referee]),
      set_current_world(Referee),
      Retry_Fun()
  end.

get_current_world() ->
  case get(current_world) of
    undefined        -> whereis(sn_server)
  ; W when is_pid(W) -> W
  end.

set_current_world(W) when is_pid(W) ->
  ?INFO("... client ~p set current world ~p~n~n", [self(), W]),
  put(current_world, W);

set_current_world(W) when is_atom(W) ->
  set_current_world(whereis(W)).

get_my_color() ->
  case get(my_color) of
    undefined -> set_my_color(generate_color()), get_my_color()
  ; C         -> C
  end.

set_my_color(C) ->
  put(my_color, C).

generate_color() ->
  {color, node(), self(), now(), make_ref()}.
