-module(process_allocator).
-export([start/1, loop/5]).

start(Limit) when is_integer(Limit) ->
  case whereis(?MODULE) of
    undefined ->
      Pid = spawn(?MODULE, loop, init(Limit)),
      true = register(?MODULE, Pid),
      io:format("~p registered~n", [?MODULE]),
      {started, Pid}
  ; Pid when is_pid(Pid) ->
      {running, Pid}
  end;

start(Fun) when is_function(Fun) ->
  whereis(?MODULE) ! {start, Fun, self()},
  receive
    {ok, {Pid, _Ref}} when is_pid(Pid) -> {ok, Pid}
  end.

init(Limit) ->
  [_Alive = 1, _Pending = [], Limit, _Max = 1, _Retired = 0].

loop(Alive, [{Fun, Pid} | Pending], Limit, Max, Retired) when Alive < Limit ->
  Pid ! {ok, spawn_monitor(Fun)},
  loop(Alive + 1, Pending, Limit, Max, Retired);

loop(Alive, Pending, Limit, Max, Retired) when Alive-Retired > Max ->
  loop(Alive, Pending, Limit, Alive-Retired, Retired);

loop(Alive, Pending, Limit, Max, Retired) ->
  receive
    {start, Fun, Pid} when is_pid(Pid) ->
      loop(Alive, [{Fun, Pid} | Pending], Limit, Max, Retired)
  ; {retire, Pid} when is_pid(Pid) ->
      loop(Alive, Pending, Limit, Max, Retired + 1)
  ; {'DOWN', _Ref, process, _Pid, _Reason} ->
      loop(Alive - 1, Pending, Limit, Max, Retired)
  ; debug ->
      io:format("process_allocator: Alive=~p, Pending=~p, Limit=~p, Max=~p, Retired=~p~n",
        [Alive, Pending, Limit, Max, Retired]),
      loop(Alive, Pending, Limit, Max, Retired)
  end.
