-module(jill).
-export([start/0]).
-import(sn, [speculate/2, cm/1]).
-import(trade, [buy/2, sell/2]).

start() ->
  speculate(
    fun(_) ->
      sell(goodlens, fun() ->
        buy(avgcam, fun() ->
          cm(fun(_) ->
            io:format("@@@ jill: goodlens => avgcam~n~n")
          end)
        end)
      end)
    end,
    fun(_) ->
      sell(goodcam, fun() ->
        buy(avgcam, fun() ->
          cm(fun(_) ->
            io:format("*** jill: goodcam => avgcam~n~n")
          end)
        end)
      end)
    end).