-module(simple_jill).
-export([start/1]).
-import(sn, [speculate/2, cm/1, in/2, out/2]).

start(Id) ->
  speculate(
    fun(W1) ->
      io:format("J ~p   L     fork  in  ~p~n", [Id, W1]),
      in({x}, fun(_, W2) ->
        io:format("J ~p   L      -x   in  ~p~n", [Id, W2]),
        cm(fun(W3) ->
          io:format("J ~p   L      cm   in  ~p ^^^~n", [Id, W3]),
          out({jill, Id, left}, fun(_) -> ok end)
        end)
      end)
    end,
    fun(W1) ->
      io:format("J ~p     R   fork  in  ~p~n", [Id, W1]),
      in({z}, fun(_, W2) ->
        io:format("J ~p     R    -z   in  ~p~n", [Id, W2]),
        cm(fun(W3) ->
          io:format("J ~p     R    cm   in  ~p !!!~n", [Id, W3]),
          out({jill, Id, right}, fun(_) -> ok end)
        end)
      end)
    end).