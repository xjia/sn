-module(simple_bob).
-export([start/1]).
-import(sn, [speculate/2, cm/1, in/2, out/2]).

start(Id) ->
  speculate(
    fun(W1) ->
      io:format("B ~p   L     fork  in  ~p~n", [Id, W1]),
      out({x}, fun(W2) ->
        io:format("B ~p   L      +x   in  ~p~n", [Id, W2]),
        cm(fun(W3) ->
          io:format("B ~p   L      cm   in  ~p~n", [Id, W3])
        end)
      end)
    end,
    fun(W1) ->
      io:format("B ~p     R   fork  in  ~p~n", [Id, W1]),
      out({y}, fun(W2) ->
        io:format("B ~p     R    +y   in  ~p~n", [Id, W2]),
        cm(fun(W3) ->
          io:format("B ~p     R    cm   in  ~p~n", [Id, W3])
        end)
      end)
    end).