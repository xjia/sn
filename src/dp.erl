-module(dp).
-export([start/1]).
-import(sn, [speculate/2, in/2, out/2, cm/1]).

start({I, N}) ->
  L = I-1,
  R = I rem N,
  speculate(
    fun(_) ->
      in({fork, L}, fun(_, _) ->
        in({fork, R}, fun(_, _) ->
          cm(fun(_) ->
            out({fork, L}, fun(_) ->
              out({fork, R}, fun(_) ->
                {Down_Time, _} = statistics(runtime),
                io:format("DP ~p: Down_Time=~p~n", [I, Down_Time])
              end)
            end)
          end)
        end)
      end)
    end,
    fun(_) ->
      in({fork, R}, fun(_, _) ->
        in({fork, L}, fun(_, _) ->
          cm(fun(_) ->
            out({fork, L}, fun(_) ->
              out({fork, R}, fun(_) ->
                {Down_Time, _} = statistics(runtime),
                io:format("DP ~p: Down_Time=~p~n", [I, Down_Time])
              end)
            end)
          end)
        end)
      end)
    end).
