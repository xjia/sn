-module(tsell).
-export([start/1]).

-define(TICK, 100).

sell([]) -> io:format("all tickets sold out~n");
sell([{X,Y,Z}|Ts]) ->
  timer:sleep(?TICK),
  sn:out({ticket,X,Y,Z}, fun(_) -> sell(Ts) end).

start(Tickets) -> sell(Tickets).
