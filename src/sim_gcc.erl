-module(sim_gcc).  % simulation of generalized committed choice
-export([start/0, loop/1, schedule/0]).
-export([start/2, choice/2, in/2, rd/2, out/2, cm/1]).

start() ->
  Pid = spawn(?MODULE, loop, init()),
  register(?MODULE, Pid),
  Pid.

init() -> [_Processes = []].

loop(Ps) ->
  receive
    {start, Module, Param} ->
      {P,_} = spawn_monitor(Module, start, [Param]),
      loop(Ps++[P])
  ; schedule ->
      {A1,A2,A3} = now(),
      random:seed(A1, A2, A3),
      T0 = gb_trees:empty(),
      T1 = gb_trees:insert(1, {1,leaf,[],x}, T0),
      loop(Ps, T1, [], 0, idle)
  end.

schedule() -> ?MODULE ! schedule.

loop([], Ts, [], _, idle) ->
  io:format("finalize: Ts=~p~n", [Ts]),
  init:stop();

loop([], Ts, _, 0, idle) ->
  io:format("stuck: Ts=~p~n", [Ts]),
  init:stop();

loop([], Ts, Px, _, idle) ->
  loop(Px, Ts, [], 0, idle);

loop([P|Ps], Ts, Px, Progress, idle) ->
  P ! next_step,
  loop([P|Ps], Ts, Px, Progress, waiting);

loop([P|Ps], Ts, Px, Progress, waiting) ->
  receive
    {start, Module, Param} ->
      {S,_} = spawn_monitor(Module, start, [Param]),
      loop([P|Ps]++[S], Ts, Px, Progress+1, waiting)
  ; {spawn, Fun} ->
      {S,_} = spawn_monitor(Fun),
      loop([P|Ps]++[S], Ts, Px, Progress+1, waiting)
  ; {'DOWN', _Ref, process, Pid, _Info} ->
      loop(delete_all(Pid,[P|Ps]), Ts, delete_all(Pid,Px), Progress+1, waiting)
  ; {I, choice, C} ->
      case gb_trees:get(I, Ts) of
        {I, tree, L, R, _} ->
          P ! {retry, [L, R]},
          loop(Ps, Ts, Px++[P], Progress+1, idle)
      ; {I, leaf, Tuples, C0} ->
          L = max_id(Ts) + 1,
          R = L + 1,
          P ! {ok, [L, R]},
          X1 = gb_trees:update(I, {I, tree, L, R, C0}, Ts),
          X2 = gb_trees:insert(L, {L, leaf, Tuples, C}, X1),
          X3 = gb_trees:insert(R, {R, leaf, Tuples, C}, X2),
          loop(Ps, X3, Px++[P], Progress+1, idle)
      ; {I, retired, R} ->
          P ! {retry, R},
          loop(Ps, Ts, Px++[P], Progress+1, idle)
      ; {I, killed} ->
          exit(P, kill),
          loop(Ps, Ts, Px, Progress, idle)
      end
  ; {I, in, Template} ->
      case gb_trees:get(I, Ts) of
        {I, tree, L, R, _} ->
          P ! {retry, [L, R]},
          loop(Ps, Ts, Px++[P], Progress+1, idle)
      ; {I, leaf, Tuples, C} ->
        case util:match_tuple(Template, Tuples) of
          {ok, Tuple} ->
            P ! {ok, Tuple},
            loop(Ps,
                 gb_trees:update(I, {I, leaf, lists:delete(Tuple,Tuples), C}, Ts),
                 Px++[P],
                 Progress+1,
                 idle)
        ; nothing ->
            P ! {error, wait},
            loop(Ps, Ts, Px++[P], Progress, idle)
        end
      ; {I, retired, R} ->
          P ! {retry, R},
          loop(Ps, Ts, Px++[P], Progress+1, idle)
      ; {I, killed} ->
          exit(P, kill),
          loop(Ps, Ts, Px, Progress, idle)
      end
  ; {I, rd, Template} ->
      case gb_trees:get(I, Ts) of
        {I, tree, L, R, _} ->
          P ! {retry, [L, R]},
          loop(Ps, Ts, Px++[P], Progress+1, idle)
      ; {I, leaf, Tuples, _} ->
        case util:match_tuple(Template, Tuples) of
          {ok, Tuple} ->
            P ! {ok, Tuple},
            loop(Ps, Ts, Px++[P], Progress+1, idle)
        ; nothing ->
            P ! {error, wait},
            loop(Ps, Ts, Px++[P], Progress, idle)
        end
      ; {I, retired, R} ->
          P ! {retry, R},
          loop(Ps, Ts, Px++[P], Progress+1, idle)
      ; {I, killed} ->
          exit(P, kill),
          loop(Ps, Ts, Px, Progress, idle)
      end
  ; {I, out, Tuple} ->
      case gb_trees:get(I, Ts) of
        {I, tree, L, R, _} ->
          P ! {retry, [L, R]},
          loop(Ps, Ts, Px++[P], Progress+1, idle)
      ; {I, leaf, Tuples, C} ->
          P ! ok,
          loop(Ps,
               gb_trees:update(I, {I, leaf, Tuples++[Tuple], C}, Ts),
               Px++[P],
               Progress+1,
               idle)
      ; {I, retired, R} ->
          P ! {retry, R},
          loop(Ps, Ts, Px++[P], Progress+1, idle)
      ; {I, killed} ->
          exit(P, kill),
          loop(Ps, Ts, Px, Progress, idle)
      end
  ; {I, cm, C} ->
      case gb_trees:get(I, Ts) of
        {I, tree, L, R, _} ->
          P ! {retry, [L, R]},
          loop(Ps, Ts, Px++[P], Progress+1, idle)
      ; {I, leaf, Tuples, C} ->
          X = lists:filter(fun(T) -> element(2,T) =:= tree end, gb_trees:values(Ts)),
          case lists:keyfind(I, 3, X) of
            {Parent, tree, I, R, C0} ->
              X1 = gb_trees:update(Parent, {Parent, leaf, Tuples, C0}, Ts),
              X2 = gb_trees:update(I, {I, retired, Parent}, X1),
              X3 = kill_tree(R, X2),
              P ! {ok, Parent},
              loop(Ps, X3, Px++[P], Progress+1, idle)
          ; false ->
              case lists:keyfind(I, 4, X) of
                {Parent, tree, L, I, C0} ->
                  X1 = gb_trees:update(Parent, {Parent, leaf, Tuples, C0}, Ts),
                  X2 = gb_trees:update(I, {I, retired, Parent}, X1),
                  X3 = kill_tree(L, X2),
                  P ! {ok, Parent},
                  loop(Ps, X3, Px++[P], Progress+1, idle)
              end
          end
      ; {I, leaf, _Tuples, _} ->
          P ! {error, wait},
          loop(Ps, Ts, Px++[P], Progress, idle)
      ; {I, retired, R} ->
          P ! {retry, R},
          loop(Ps, Ts, Px++[P], Progress+1, idle)
      ; {I, killed} ->
          exit(P, kill),
          loop(Ps, Ts, Px, Progress, idle)
      end
  end.

%%%

delete_all(E, L) -> [X || X <- L, X =/= E].

max_id(Ts) ->
  case gb_trees:is_empty(Ts) of
    true ->
      0
  ; false ->
      {Key, _} = gb_trees:largest(Ts),
      Key
  end.

kill_tree(I, Ts) ->
  case gb_trees:get(I, Ts) of
    {I, tree, L, R, _} ->
      X1 = kill_tree(L, Ts),
      X2 = kill_tree(R, X1),
      gb_trees:update(I, {I, killed}, X2)
  ; _Other ->
      gb_trees:update(I, {I, killed}, Ts)
  end.

%%%

start(Module, Param) -> ?MODULE ! {start, Module, Param}.

choice(Left, Right) ->
  receive
    next_step ->
      ?MODULE ! {getw(), choice, C=getc()},
      receive
        {retry, [L, R]} ->
          ?MODULE ! {spawn, fun() ->
            setw(L),
            setc(C),
            choice(Left, Right)
          end},
          setw(R),
          choice(Left, Right)
      ; {retry, R} ->
          setw(R),
          choice(Left, Right)
      ; {ok, [L, R]} ->
          ?MODULE ! {spawn, fun() ->
            setw(L),
            setc(C),
            Left()
          end},
          setw(R),
          Right()
      end
  end.

in(Template, Next) ->
  receive
    next_step ->
      ?MODULE ! {getw(), in, Template},
      receive
        {retry, [L, R]} ->
          C = getc(),
          ?MODULE ! {spawn, fun() ->
            setw(L),
            setc(C),
            in(Template, Next)
          end},
          setw(R),
          in(Template, Next)
      ; {retry, R} ->
          setw(R),
          in(Template, Next)
      ; {ok, Tuple} ->
          Next(Tuple)
      ; {error, wait} ->
          in(Template, Next)
      end
  end.

rd(Template, Next) ->
  receive
    next_step ->
      ?MODULE ! {getw(), rd, Template},
      receive
        {retry, [L, R]} ->
          C = getc(),
          ?MODULE ! {spawn, fun() ->
            setw(L),
            setc(C),
            rd(Template, Next)
          end},
          setw(R),
          rd(Template, Next)
      ; {retry, R} ->
          setw(R),
          rd(Template, Next)
      ; {ok, Tuple} ->
          Next(Tuple)
      ; {error, wait} ->
          rd(Template, Next)
      end
  end.

out(Tuple, Next) ->
  receive
    next_step ->
      ?MODULE ! {getw(), out, Tuple},
      receive
        {retry, [L, R]} ->
          C = getc(),
          ?MODULE ! {spawn, fun() ->
            setw(L),
            setc(C),
            out(Tuple, Next)
          end},
          setw(R),
          out(Tuple, Next)
      ; {retry, R} ->
          setw(R),
          out(Tuple, Next)
      ; ok ->
          Next()
      end
  end.

cm(Next) ->
  receive
    next_step ->
      ?MODULE ! {getw(), cm, getc()},
      receive
        {retry, [L, R]} ->
          C = getc(),
          ?MODULE ! {spawn, fun() ->
            setw(L),
            setc(C),
            cm(Next)
          end},
          setw(R),
          cm(Next)
      ; {retry, R} ->
          setw(R),
          cm(Next)
      ; {ok, Parent} ->
          setw(Parent),
          Next()
      ; {error, wait} ->
          cm(Next)
      end
  end.

%%%

getw() ->
  case get(current_world) of
    undefined -> setw(1)
  ; W -> W
  end.

setw(W) ->
  put(current_world, W),
  W.

getc() ->
  case get(current_color) of
    undefined -> setc(generate_color())
  ; C -> C
  end.

setc(C) ->
  put(current_color, C),
  C.

generate_color() ->
  {color, self()}.
