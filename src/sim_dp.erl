-module(sim_dp).
-export([start/1]).

start({I, N, Sim}) ->
  L = I-1,
  R = I rem N,
  Sim:rd({dp, ready}, fun(_) ->
    Sim:choice(
      fun() ->
        Sim:in({fork, L}, fun(_) ->
          Sim:in({fork, R}, fun(_) ->
            Sim:out({fork, L}, fun() ->
              Sim:out({fork, R}, fun() ->
                Sim:cm(fun() ->
                  ok
                end)
              end)
            end)
          end)
        end)
      end,
      fun() ->
        Sim:in({fork, R}, fun(_) ->
          Sim:in({fork, L}, fun(_) ->
            Sim:out({fork, L}, fun() ->
              Sim:out({fork, R}, fun() ->
                Sim:cm(fun() ->
                  ok
                end)
              end)
            end)
          end)
        end)
      end)
  end).
