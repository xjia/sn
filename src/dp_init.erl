-module(dp_init).
-export([start/1]).

start(N) -> dp_init(0, N).

dp_init(N, N) -> util:repeat(N, fun(I) -> sn:start(dp, {I, N}) end);
dp_init(I, N) -> sn:out({fork, I}, fun(_) -> dp_init(I + 1, N) end).
