-module(sn_server).
-export([start/0, loop/1]).
-include("connection.hrl").

-define(INFO(_Format, _Params), ok).
% -define(INFO(Format, Params), io:format(Format, Params)).

%%%%%%%%%%%%%%%%%
%% server-side %%
%%%%%%%%%%%%%%%%%

start() ->
  case whereis(?MODULE) of
    undefined ->
      Pid = spawn(?MODULE, loop, init()),
      true = register(?MODULE, Pid),
      io:format("~p registered~n", [?MODULE]),
      sn_server_monitor:start(Pid),
      {started, Pid}
  ; Pid when is_pid(Pid) ->
      {running, Pid}
  end.

%%%%%%%%%%%%%%%%%%%%%%%%
%% record definitions %%
%%%%%%%%%%%%%%%%%%%%%%%%

-record(leaf_root, {store, ins, rds}).
-record(root, {children}).
-record(leaf, {parent, color, store, commits, ins, rds}).
-record(interior, {parent, children, color}).
-record(retired, {referee}).

-record(waiting_in, {connection, template}).
-record(waiting_rd, {connection, template}).
-record(commit, {connection, color}).

%%%%%%%%%%%%%%%%%%%%%%%%
%% internal functions %%
%%%%%%%%%%%%%%%%%%%%%%%%

init() ->
  [#leaf_root{store=[], ins=[], rds=[]}].

loop(State) ->
  receive
    {remote, Conn, Msg} ->
      case handle(Conn, Msg, State) of
        {reply, Reply} ->
          ?INFO(">>> Msg from ~p to ~p~n    ~p~n<<< ~p~n~n", [Conn#connection.pid, self(), Msg, Reply]),
          remote:reply(Conn, Reply),
          loop(State)
      ; {reply, Reply, New_State} ->
          ?INFO(">>> Msg from ~p to ~p~n    ~p~n<<< ~p~n~n", [Conn#connection.pid, self(), Msg, Reply]),
          remote:reply(Conn, Reply),
          loop(New_State)
      ; {noreply, New_State} ->
          ?INFO(">>> Msg from ~p to ~p~n    ~p~n<<< (noreply)~n~n", [Conn#connection.pid, self(), Msg]),
          loop(New_State)
      end
  ; {replace, Store, Ins, Rds, Pid} ->
      case handle_replace(State, Store, Ins, Rds, Pid) of
        {reply, Reply} ->
          Pid ! Reply,
          loop(State)
      ; {reply, Reply, New_State} ->
          Pid ! Reply,
          loop(New_State)
      ; {noreply, New_State} ->
          loop(New_State)
      end
  ; {debug, dump, Pid} ->
      Pid ! {debug, dump, State},
      loop(State)
  end.

reply(Reply) ->
  {reply, Reply}.

reply(Reply, New_State) ->
  {reply, Reply, New_State}.

noreply(New_State) ->
  {noreply, New_State}.

%%%%%%%%%%%%%%%
%% leaf root %%
%%%%%%%%%%%%%%%

handle(_, {split, Color}, #leaf_root{store=Store, ins=Ins, rds=Rds}) ->
  Children = [spawn_child(Color, Store), spawn_child(Color, Store)],
  lists:map(fun(#waiting_in{connection=Conn}) -> remote:reply(Conn, {retry, Children}) end, Ins),
  lists:map(fun(#waiting_rd{connection=Conn}) -> remote:reply(Conn, {retry, Children}) end, Rds),
  reply({ok, Children}, #root{children=Children});

handle(Conn, {in, Template}, State=#leaf_root{store=Store, ins=Ins}) ->
  case util:match_tuple(Template, Store) of
    {ok, Tuple} ->
      reply({ok, Tuple}, State#leaf_root{store=lists:delete(Tuple, Store)})
  ; _NotFound ->
      noreply(State#leaf_root{ins=[#waiting_in{connection=Conn, template=Template} | Ins]})
  end;

handle(Conn, {rd, Template}, State=#leaf_root{store=Store, rds=Rds}) ->
  case util:match_tuple(Template, Store) of
    {ok, Tuple} ->
      reply({ok, Tuple})
  ; _NotFound ->
      noreply(State#leaf_root{rds=[#waiting_rd{connection=Conn, template=Template} | Rds]})
  end;

handle(_, {out, Tuple}, #leaf_root{store=Store, ins=Ins, rds=Rds}) ->
  {S, I, R} = add_tuple(Store, Ins, Rds, Tuple),
  reply(ok, #leaf_root{store=S, ins=I, rds=R});

%%%%%%%%%%
%% root %%
%%%%%%%%%%

handle(_, {_, _}, #root{children=Children}) ->
  reply({retry, Children});

%%%%%%%%%%
%% leaf %%
%%%%%%%%%%

handle(_, {split, Color}, State=#leaf{store=Store, commits=Commits, ins=Ins, rds=Rds}) ->
  Children = [spawn_child(Color, Store), spawn_child(Color, Store)],
  lists:map(fun(#waiting_in{connection=Conn}) -> remote:reply(Conn, {retry, Children}) end, Ins),
  lists:map(fun(#waiting_rd{connection=Conn}) -> remote:reply(Conn, {retry, Children}) end, Rds),
  lists:map(fun(#commit{connection=Conn}) -> remote:reply(Conn, {retry, Children}) end, Commits),
  reply({ok, Children}, #interior{parent=State#leaf.parent, children=Children, color=State#leaf.color});

handle(_, {commit, Color}, #leaf{parent=Parent, color=Color, store=Store, commits=Commits, ins=Ins, rds=Rds}) ->
  Parent ! {replace, Store, Ins, Rds, self()},
  receive
    replaced ->
      lists:map(fun(#commit{connection=Conn}) -> remote:reply(Conn, {retry, Parent}) end, Commits),
      whereis(process_allocator) ! {retire, self()},
      reply({ok, Parent}, #retired{referee=Parent})
  end;

handle(Conn, {commit, Color}, State=#leaf{commits=Commits}) ->
  noreply(State#leaf{commits=[#commit{connection=Conn, color=Color} | Commits]});

handle(Conn, {in, Template}, State=#leaf{store=Store, ins=Ins}) ->
  case util:match_tuple(Template, Store) of
    {ok, Tuple} ->
      reply({ok, Tuple}, State#leaf{store=lists:delete(Tuple, Store)})
  ; _NotFound ->
      noreply(State#leaf{ins=[#waiting_in{connection=Conn, template=Template} | Ins]})
  end;

handle(Conn, {rd, Template}, State=#leaf{store=Store, rds=Rds}) ->
  case util:match_tuple(Template, Store) of
    {ok, Tuple} ->
      reply({ok, Tuple})
  ; _NotFound ->
      noreply(State#leaf{rds=[#waiting_rd{connection=Conn, template=Template} | Rds]})
  end;

handle(_, {out, Tuple}, State=#leaf{store=Store, ins=Ins, rds=Rds}) ->
  {S, I, R} = add_tuple(Store, Ins, Rds, Tuple),
  reply(ok, State#leaf{store=S, ins=I, rds=R});

%%%%%%%%%%%%%%
%% interior %%
%%%%%%%%%%%%%%

handle(_, {_, _}, #interior{children=Children}) ->
  reply({retry, Children});

%%%%%%%%%%%%%
%% retired %%
%%%%%%%%%%%%%

handle(_, {_, _}, #retired{referee=Referee}) ->
  reply({retry, Referee});

%%%%%%%%%%%%%%%%%%%%%
%% catch-all error %%
%%%%%%%%%%%%%%%%%%%%%

handle(Conn, Msg, State) -> {not_supported, Conn, Msg, State}.

%%%%%%%%%%%%%%%%%%%%%%%%%%
%% handle replace, etc. %%
%%%%%%%%%%%%%%%%%%%%%%%%%%

handle_replace(#root{children=Children}, Store, Ins, Rds, Pid) ->
  lists:map(fun(Child) -> kill_child(Child) end, lists:delete(Pid, Children)),
  reply(replaced, #leaf_root{store=Store, ins=Ins, rds=Rds});

handle_replace(#interior{parent=Parent, children=Children, color=Color}, Store, Ins, Rds, Pid) ->
  lists:map(fun(Child) -> kill_child(Child) end, lists:delete(Pid, Children)),
  reply(replaced, #leaf{parent=Parent, color=Color, store=Store, commits=[], ins=Ins, rds=Rds});

handle_replace(State, _, _, _, _) ->
  noreply(State).

%%%%%%%%%%%%%%%%%%%%%%
%% helper functions %%
%%%%%%%%%%%%%%%%%%%%%%

spawn_child(Color, Store) ->
  Self = self(),
  {ok, Pid} = process_allocator:start(fun() ->
    loop(#leaf{parent=Self, color=Color, store=Store, commits=[], ins=[], rds=[]})
  end),
  link(Pid),
  Pid.

kill_child(Child) ->
  unlink(Child),
  receive
    {'EXIT', Child, _} -> true
    after 0            -> true
  end,
  exit(Child, kill).

add_tuple(Store, Ins, Rds, Tuple) ->
  R = check_waiting_rds(Rds, Tuple),
  {S, I} = check_waiting_ins({Store, Ins}, Tuple),
  {S, I, R}.

check_waiting_rds(Rds, Tuple) ->
  check_waiting_rds([], Rds, Tuple).

check_waiting_rds(Waiting, [], _Tuple) ->
  Waiting;
check_waiting_rds(Waiting, [Rd=#waiting_rd{connection=Conn, template=Template} | Rest], Tuple) ->
  case util:matches(Template, Tuple) of
    true ->
      remote:reply(Conn, {ok, Tuple}),
      check_waiting_rds(Waiting, Rest, Tuple)
  ; _NotMatch ->
      check_waiting_rds([Rd | Waiting], Rest, Tuple)
  end.

check_waiting_ins({Store, Ins}, Tuple) ->
  case check_waiting_ins([], Ins, Tuple) of
    {matched, Waiting} ->
      {Store, Waiting}
  ; not_matched ->
      {[Tuple | Store], Ins}
  end.

check_waiting_ins(_Waiting, [], _Tuple) ->
  not_matched;
check_waiting_ins(Waiting, [In=#waiting_in{connection=Conn, template=Template} | Rest], Tuple) ->
  case util:matches(Template, Tuple) of
    true ->
      remote:reply(Conn, {ok, Tuple}),
      {matched, lists:append(Waiting, Rest)}
  ; _NotMatch ->
      check_waiting_ins([In | Waiting], Rest, Tuple)
  end.
