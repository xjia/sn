-module(sim_stock_ticker).
-export([start/1]).

start({X, Sim}) ->
  Sim:out({tick, X, X, X}, fun() ->
    Sim:in({ack, tick}, fun(_) -> start({X, Sim}) end)
  end).
