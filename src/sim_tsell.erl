-module(sim_tsell).
-export([start/1]).

sell([], _) -> io:format("all tickets sold out~n");
sell([{X,Y,Z}|Ts], Sim) ->
  Sim:out({ticket,X,Y,Z}, fun() -> sell(Ts, Sim) end).

start({Tickets, Sim}) -> sell(Tickets, Sim).
