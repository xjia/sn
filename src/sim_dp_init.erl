-module(sim_dp_init).
-export([start/1]).

start({N, Sim}) -> dp_init(0, N, Sim).

dp_init(N, N, Sim) -> Sim:out({dp, ready}, fun() -> ok end);
dp_init(I, N, Sim) -> Sim:out({fork, I}, fun() -> dp_init(I + 1, N, Sim) end).
