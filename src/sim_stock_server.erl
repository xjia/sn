-module(sim_stock_server).
-export([start/1]).
-include("sn.hrl").

start({Stock, Start_Quantity, Prices=[P|_], Sim}) ->
  Sim:out({price, Stock, P}, fun() ->
    serve(Stock, Start_Quantity, Prices, 0, [], 1, Sim)
  end).

serve(X, Q, [], K, Passed, Day, Sim) ->
  serve(X, Q, Passed, K, [], Day, Sim);

serve(X, Q, [P|Ps=[NewPrc|_]], 1, Passed, Day, Sim) ->
  Sim:in({price, X, ?Any}, fun(_) ->
    Sim:out({price, X, NewPrc}, fun() ->
      serve(X, Q, Ps, 0, [P|Passed], Day+1, Sim)
    end)
  end);

serve(X, Q, Ps=[P|_], K, Passed, Day, Sim) ->
  Sim:in({?Any, X, ?Any, ?Any}, fun
    ({tick, _, _, _}) ->
      Sim:out({ack, tick}, fun() ->
        serve(X, Q, Ps, K+1, Passed, Day, Sim)
      end)
  ; ({buy, _, Q1, Me}) ->
      Q2 = min(Q1, Q),
      Sim:out({ack, Me, P, Q2}, fun() ->
        serve(X, Q-Q2, Ps, K+1, Passed, Day, Sim)
      end)
  ; ({sell, _, Q2, Me}) ->
      Sim:out({ack, Me, P}, fun() ->
        serve(X, Q+Q2, Ps, K, Passed, Day, Sim)
      end)
  end).
