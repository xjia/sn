-module(delay).
-export([execute/0, clear/0]).
-export([set_delay/1]).

-define(INIT_DELAY, 25).
-define(MAX_DELAY, 60 * 1000).

-define(INFO(_Format, _Params), ok).

execute() ->
  Delay = get_delay(),
  ?INFO("~p is going to sleep for ~pms~n", [self(), Delay]),
  receive
    after Delay -> ?INFO("~p wakes up after sleeping for ~pms~n", [self(), Delay])
  end,
  New_Delay = Delay * 2 + random:uniform(10),
  set_delay(New_Delay),
  New_Delay.

clear() ->
  erase(my_delay).

get_delay() ->
  case get(my_delay) of
    undefined -> ?INIT_DELAY
  ; Delay -> Delay
  end.

set_delay(Delay) when Delay =< ?MAX_DELAY ->
  put(my_delay, Delay);
set_delay(Delay) when Delay > ?MAX_DELAY ->
  clear().
