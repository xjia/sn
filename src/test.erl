-module(test).
-export([st/1, fr_count/1, fr/1, fr_prepare/2, dp/1, bj/1, sbj/1]).
-export([dump/0, dump/1]).
-export([try_link/0, try_sleep/0, try_spawn_link/0, try_exit/0]).

-define(PROCESS_LIMIT, 1000000).

-include("prices_apple.hrl").
-include("prices_apricot.hrl").
-include("prices_babaco.hrl").
-include("prices_banana.hrl").
-include("prices_cashew.hrl").
-include("prices_coconut.hrl").
-include("prices_durian.hrl").
-include("prices_lime.hrl").
-include("prices_mango.hrl").
-include("prices_orange.hrl").
st(N) ->
  sn_app:start(memory_monitor),
  sn_app:start(process_allocator, ?PROCESS_LIMIT),
  sn_app:start(sn_server),
  sn_app:start(client_monitor, N),
  stock_server:start(apple,   100 * N, ?APPLE),
  stock_server:start(apricot, 100 * N, ?APRICOT),
  stock_server:start(babaco,  100 * N, ?BABACO),
  stock_server:start(banana,  100 * N, ?BANANA),
  stock_server:start(cashew,  100 * N, ?CASHEW),
  stock_server:start(coconut, 100 * N, ?COCONUT),
  stock_server:start(durian,  100 * N, ?DURIAN),
  stock_server:start(lime,    100 * N, ?LIME),
  stock_server:start(mango,   100 * N, ?MANGO),
  stock_server:start(orange,  100 * N, ?ORANGE),
  Stocks = [apple, apricot, babaco, banana, cashew, coconut, durian, lime, mango, orange],
  util:repeat(N, fun(I) ->
    sn:start(stock_trader, {I, 0.01, 0.05, util:shuffle(Stocks)})
  end),
  stock_finalizer:start(N).

-include("ha30.hrl").
fr_count(N) ->
  Routes = lists:sublist(util:shuffle(?HA30_TBUY), N),
  io:format("~p~n", [length(util:shuffle(fr_prepare(?HA30_DIST, Routes)))]).

fr(N) ->
  sn_app:start(memory_monitor),
  sn_app:start(process_allocator, ?PROCESS_LIMIT),
  sn_app:start(sn_server),
  sn_app:start(client_monitor, N + 1),
  Routes = lists:sublist(util:shuffle(?HA30_TBUY), N),
  sn:start(tsell, util:shuffle(fr_prepare(?HA30_DIST, Routes))),
  util:repeat(N, fun(I) -> sn:start(tbuy, {I, lists:nth(I, Routes)}) end),
  tbuy_finalizer:start(N).

fr_prepare(Ts, Rs) -> fr_prepare(Ts, Rs, []).

fr_prepare(_, [], Ans) -> Ans;
fr_prepare(Ts, [{X,Y,Cash,[Y,Z|_]}|Rs], Ans) ->
  fr_prepare(Ts, Rs, [{X,Y,Cash},{X,Z,price_of(X,Z,Ts)},{Z,Y,price_of(Z,Y,Ts)},{X,Y,Cash},{X,Z,price_of(X,Z,Ts)},{Z,Y,price_of(Z,Y,Ts)}|Ans]).

price_of(X,Y,[{X,Y,Z}|_]) -> Z;
price_of(X,Y,[{Y,X,Z}|_]) -> Z;
price_of(X,Y,[_|Ts]) -> price_of(X,Y,Ts).

dp(N) ->
  sn_app:start(memory_monitor),
  sn_app:start(process_allocator, ?PROCESS_LIMIT),
  sn_app:start(sn_server),
  sn_app:start(client_monitor, N + 1),
  sn:start(dp_init, N).

bj(N) ->
  sn_app:start(memory_monitor),
  sn_app:start(process_allocator, ?PROCESS_LIMIT),
  sn_app:start(sn_server),
  sn_app:start(client_monitor, N * 2),
  util:repeat(N, fun(_) -> sn:start(bob), sn:start(jill) end).

sbj(N) ->
  sn_app:start(memory_monitor),
  sn_app:start(process_allocator, ?PROCESS_LIMIT),
  sn_app:start(sn_server),
  sn_app:start(client_monitor, N * 2),
  util:repeat(N, fun(I) -> sn:start(simple_bob, I), sn:start(simple_jill, I) end).

dump() ->
  dump(whereis(sn_server)).

dump(N) when is_integer(N) ->
  dump(c:pid(0, N, 0));

dump(Pid) when is_pid(Pid) ->
  Pid ! {debug, dump, self()},
  receive
    {debug, dump, State} ->
      io:format("~p~n", [State])
  end.

try_link() ->
  Pid1 = spawn(fun() ->
    receive
      stop -> exit(kill)
    end
  end),
  Pid2 = spawn(fun() ->
    link(Pid1),
    receive
      stop -> exit(kill)
    end
  end),
  {Pid1, Pid2}.

try_sleep() ->
  receive
    killed -> kill
  after
    60000 -> timeout
  end.

try_spawn_link() ->
  spawn(fun() ->
    io:format("parent: ~p~n", [Parent = self()]),
    Client = spawn_link(fun() ->
      io:format("child: ~p~n", [self()]),
      spawn_link_loop(Parent)
    end),
    spawn_link_loop(Client)
  end).

spawn_link_loop(Peer) ->
  receive
    unlink ->
      unlink(Peer)
  ; quit ->
      exit(shutdown)
  ; Any ->
      io:format("~p receives ~p~n", [self(), Any])
  end,
  spawn_link_loop(Peer).

try_exit() ->
  spawn(fun() ->
    Parent = self(),
    Child = spawn_link(fun() -> spawn_link_loop(Parent) end),
    io:format("Child is ~p~n", [Child]),
    try_exit_parent(Child)
  end).

try_exit_parent(Child) ->
  receive
    unlink ->
      unlink(Child)
  ; kill ->
      exit(Child, kill)
  ; quit ->
      exit(shutdown)
  ; Any ->
      io:format("~p receives ~p~n", [self(), Any])
  end,
  try_exit_parent(Child).
