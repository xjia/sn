-module(remote).
-export([send/2, reply/2]).
-include("connection.hrl").

ensure_alive(Pid) when is_pid(Pid) ->
  case is_process_alive(Pid) of
    true  -> ok
  ; false -> exit(connection_lost)
  end.

send(Target, Msg) when is_atom(Target) ->
  send(whereis(Target), Msg);

send(Target, Msg) when is_pid(Target) ->
  Ticket = util:generate_unique_id(),
  ensure_alive(Target),
  % link(Target),
  Target ! {?MODULE, #connection{pid=self(), ticket=Ticket}, Msg},
  util:backoff(fun(Timeout) ->
    ensure_alive(Target),
    receive
      {?MODULE, Ticket, Reply} ->
        {ok, Reply}
      after Timeout ->
        backoff
    end
  end).

reply(#connection{pid=Pid, ticket=Ticket}, Reply) ->
  % unlink(Pid),
  % receive
  %   {'EXIT', Pid, _} -> true
  %   after 0          -> true
  % end,
  Pid ! {?MODULE, Ticket, Reply}.
