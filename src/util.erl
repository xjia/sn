-module(util).
-export([replace/3, repeat/2, shuffle/1, floor/1, ceiling/1]).
-export([backoff/1, backoff/2, backoff/3, backoff/4]).
-export([generate_unique_id/0, generate_unique_md5/0]).
-export([match_tuple/2, matches/2]).

replace([],    _, _) -> [];
replace([X|T], X, Y) -> [Y|T];
replace([Z|T], X, Y) -> [Z|replace(T, X, Y)].

repeat(0, _) -> ok;
repeat(N, F) when N > 0 -> F(N), repeat(N - 1, F).

shuffle(L) -> [X || {_,X} <- lists:sort([ {random:uniform(),N} || N<-L ])].

floor(X) when X < 0 ->
  T = trunc(X),
  case X - T == 0 of
    true  -> T
  ; false -> T - 1
  end;
floor(X) -> 
  trunc(X).

ceiling(X) when X < 0 ->
  trunc(X);
ceiling(X) ->
  T = trunc(X),
  case X - T == 0 of
    true  -> T
  ; false -> T + 1
  end.


% exponential backoff

backoff(F) ->
  backoff(100, F).

backoff(Max, F) ->
  backoff(1, Max, F).

backoff(Initial, Max, F) ->
  backoff(Initial, Max, 2, F).

backoff(Initial, Max, Ratio, F) ->
  backoff(Initial, Initial, Max, Ratio, F).

backoff(Initial, Now, Max, Ratio, F) when Now =< Max ->
  case F(Now) of
    {ok, Result} -> Result
  ; backoff      -> backoff(Initial, Now * Ratio, Max, F)
  end;
backoff(Initial, Now, Max, Ratio, F) when Now > Max ->
  backoff(Initial, Max, Ratio, F).


generate_unique_id() ->
  {node(), self(), now(), make_ref()}.

generate_unique_md5() ->
  hexify(crypto:md5(term_to_binary(generate_unique_id()))).

hexify(Binary) ->
  lists:flatten([io_lib:format("~2.16.0b", [B]) || <<B>> <= Binary]).


match_tuple(_Template, []) ->
  nothing;
match_tuple(Template, [H|T]) ->
  case matches(Template, H) of
    true ->
      {ok, H}
  ; _NotMatch ->
      match_tuple(Template, T)
  end.

matches(Template, Tuple) when size(Template) =:= size(Tuple) ->
  matches(1, Template, Tuple);
matches(_Template, _Tuple) ->
  false.

matches(I, Template, _Tuple) when I > size(Template) ->
  true;
matches(I, Template, Tuple) ->
  T = element(I, Template),
  E = element(I, Tuple),
  case is_function(T, 1) of
    true ->
      case T(E) of
        true ->
          matches(I + 1, Template, Tuple)
      ; _ ->
          false
      end
  ; _ ->
      case T of
        {lt, X} ->
          case E < X of
            true  -> matches(I + 1, Template, Tuple)
          ; false -> false
          end
      ; {le, X} ->
          case E =< X of
            true  -> matches(I + 1, Template, Tuple)
          ; false -> false
          end
      ; {gt, X} ->
          case E > X of
            true  -> matches(I + 1, Template, Tuple)
          ; false -> false
          end
      ; {ge, X} ->
          case E >= X of
            true  -> matches(I + 1, Template, Tuple)
          ; false -> false
          end
      ; {btwn, X, Y} ->
          case X < E andalso E < Y of
            true  -> matches(I + 1, Template, Tuple)
          ; false -> false
          end
      ; {btwneq, X, Y} ->
          case X =< E andalso E =< Y of
            true  -> matches(I + 1, Template, Tuple)
          ; false -> false
          end
      ; {otsd, X, Y} ->
          case E < X orelse Y < E of
            true  -> matches(I + 1, Template, Tuple)
          ; false -> false
          end
      ; {otsdeq, X, Y} ->
          case E =< X orelse Y =< E of
            true  -> matches(I + 1, Template, Tuple)
          ; false -> false
          end
      ; E ->
          matches(I + 1, Template, Tuple)
      ; _ ->
          false
      end
  end.
