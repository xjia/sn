-module(sim_tbuy).
-export([start/1]).
-include("sn.hrl").

start(State={I, {X, Y, Cash, [Y,Z|_]}, Sim}) ->
  io:format("Traveler ~p: ~p~n", [I, State]),
  Sim:choice(
    fun() ->
      Sim:in({ticket, X, Y, ?LessEqual(Cash)}, fun(_) ->
        io:format("Traveler ~p: arrived by XY~n", [I])
      end)
    end,
    fun() ->
      XCash = Cash / 0.9,
      Sim:in({ticket, X, Z, ?LessEqual(XCash)}, fun({_, _, _, Cost}) ->
        YCash = (Cash - Cost) / 0.9,
        Sim:in({ticket, Z, Y, ?LessEqual(YCash)}, fun(_) ->
          io:format("Traveler ~p: arrived by XZY~n", [I])
        end)
      end)
    end).
