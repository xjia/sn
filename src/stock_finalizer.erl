-module(stock_finalizer).
-export([start/1, loop/2]).
-include("sn.hrl").

start(N) -> spawn(?MODULE, loop, [N, []]).

loop(0, Ms) -> io:format("sleep before finalize: ~p~n", [Ms]),
               timer:sleep(5000),
               io:format("finalize!~n"),
               whereis(client_monitor) ! finalize;

loop(N, Ms) -> sn:in({cash, ?Any, ?Any}, fun({_, _, M}, _) ->
                 loop(N-1, [M|Ms])
               end).
