-module(client_monitor).
-export([start/1, spawn/1, loop/4]).

start(Expected) when is_integer(Expected) ->
  sn_app:requires(memory_monitor),
  sn_app:requires(process_allocator),
  sn_app:requires(sn_server),
  case whereis(?MODULE) of
    undefined ->
      Pid = spawn(?MODULE, loop, init(Expected)),
      true = register(?MODULE, Pid),
      io:format("~p registered~n", [?MODULE]),
      {started, Pid}
  ; Pid when is_pid(Pid) ->
      {running, Pid}
  end;

start(Fun) when is_function(Fun) -> whereis(?MODULE) ! {start, Fun}.

spawn(Fun) when is_function(Fun) -> whereis(?MODULE) ! {spawn, Fun}.

init(Expected) ->
  whereis(memory_monitor) ! self(),
  receive
    {memory, Last, _} ->
      [Expected, _Started = 0, _Alive = 0, {statistics(runtime), Last}]
  end.

finalize(Start_Time, Start_Memory) ->
  {End_Time, _} = statistics(runtime),
  timer:sleep(2000),
  whereis(memory_monitor) ! self(),
  receive
    {memory, End_Memory, Peak_Memory} ->
      io:format("client_monitor: Start_Time=~p, End_Time=~p, Start_Memory=~p, End_Memory=~p, Peak_Memory=~p~n",
        [Start_Time, End_Time, Start_Memory, End_Memory, Peak_Memory]),
      io:format("client_monitor: ~p ms, ~p bytes (end) / ~p bytes (peak)~n",
        [ End_Time - Start_Time,
          End_Memory - Start_Memory,
          Peak_Memory - Start_Memory
        ]),
      whereis(process_allocator) ! debug,
      % test:dump(),
      init:stop()
  end.

loop(Expected, Expected, _Alive = 0, {{Start_Time, _}, Start_Memory}) ->
  finalize(Start_Time, Start_Memory);

loop(Expected, Started, Alive, Runtime={{Start_Time, _}, Start_Memory}) when Expected >= Started ->  
  receive
    {start, Fun} ->
      spawn_monitor(Fun),
      loop(Expected, Started + 1, Alive + 1, Runtime)
  ; {spawn, Fun} ->
      spawn_monitor(Fun),
      loop(Expected, Started, Alive + 1, Runtime)
  ; {'DOWN', _Ref, process, _Pid, normal} ->
      % {Down_Time, _} = statistics(runtime),
      % io:format(">>> client_monitor: normal down ~p~n", [Down_Time - Start_Time]),
      loop(Expected, Started, Alive - 1, Runtime)
  ; {'DOWN', _Ref, process, _Pid, _Reason} ->
      loop(Expected, Started, Alive - 1, Runtime)
  ; debug ->
      io:format(">>> client_monitor: loop(~p, ~p, ~p, ~p)~n", [Expected, Started, Alive, Runtime]),
      loop(Expected, Started, Alive, Runtime)
  ; finalize ->
      finalize(Start_Time, Start_Memory)
  end.
