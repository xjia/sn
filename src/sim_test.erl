-module(sim_test).
-export([dp/2, fr/2, st/2]).

dp(N, Sim) ->
  Sim:start(),
  Sim:start(sim_dp_init, {N, Sim}),
  util:repeat(N, fun(I) -> Sim:start(sim_dp, {I, N, Sim}) end),
  Sim:schedule(),
  ok.

-include("ha30.hrl").
fr(N, Sim) ->
  Sim:start(),
  Routes = lists:sublist(util:shuffle(?HA30_TBUY), N),
  Sim:start(sim_tsell, {util:shuffle(test:fr_prepare(?HA30_DIST, Routes)), Sim}),
  util:repeat(N, fun(I) -> Sim:start(sim_tbuy, {I, lists:nth(I, Routes), Sim}) end),
  Sim:schedule(),
  ok.

-include("prices_apple.hrl").
-include("prices_apricot.hrl").
-include("prices_babaco.hrl").
-include("prices_banana.hrl").
-include("prices_cashew.hrl").
-include("prices_coconut.hrl").
-include("prices_durian.hrl").
-include("prices_lime.hrl").
-include("prices_mango.hrl").
-include("prices_orange.hrl").
st(N, Sim) ->
  Sim:start(),
  Sim:start(sim_stock_server, {apple,   100 * N, ?APPLE, Sim}),
  Sim:start(sim_stock_ticker, {apple,   Sim}),
  Sim:start(sim_stock_server, {apricot, 100 * N, ?APRICOT, Sim}),
  Sim:start(sim_stock_ticker, {apricot, Sim}),
  Sim:start(sim_stock_server, {babaco,  100 * N, ?BABACO, Sim}),
  Sim:start(sim_stock_ticker, {babaco,  Sim}),
  Sim:start(sim_stock_server, {banana,  100 * N, ?BANANA, Sim}),
  Sim:start(sim_stock_ticker, {banana,  Sim}),
  Sim:start(sim_stock_server, {cashew,  100 * N, ?CASHEW, Sim}),
  Sim:start(sim_stock_ticker, {cashew,  Sim}),
  Sim:start(sim_stock_server, {coconut, 100 * N, ?COCONUT, Sim}),
  Sim:start(sim_stock_ticker, {coconut, Sim}),
  Sim:start(sim_stock_server, {durian,  100 * N, ?DURIAN, Sim}),
  Sim:start(sim_stock_ticker, {durian,  Sim}),
  Sim:start(sim_stock_server, {lime,    100 * N, ?LIME, Sim}),
  Sim:start(sim_stock_ticker, {lime,    Sim}),
  Sim:start(sim_stock_server, {mango,   100 * N, ?MANGO, Sim}),
  Sim:start(sim_stock_ticker, {mango,   Sim}),
  Sim:start(sim_stock_server, {orange,  100 * N, ?ORANGE, Sim}),
  Sim:start(sim_stock_ticker, {orange,  Sim}),
  Stocks = [apple, apricot, babaco, banana, cashew, coconut, durian, lime, mango, orange],
  util:repeat(N, fun(I) ->
    Sim:start(sim_stock_trader, {I, 0.01, 0.05, util:shuffle(Stocks), Sim})
  end),
  Sim:schedule(),
  ok.
