<?php
$names = file('10.trace.clean.name');
$percents = file('10.trace.clean.percent');

$x = 0;
$sum = array();
foreach ($names as $i => $name) {
  $name = trim($name);
  if (array_key_exists($name, $sum)) {
    $sum[$name] += trim($percents[$i]);
  } else {
    $sum[$name] = trim($percents[$i]);
  }
  $x += $percents[$i];
}

foreach ($sum as $key => $value) {
  echo "$key\t$value\n";
}
echo $x;
